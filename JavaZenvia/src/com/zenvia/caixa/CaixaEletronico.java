package com.zenvia.caixa;

import java.util.ArrayList;

/**
 * Desenvolva um programa que simule a entrega de notas quando um cliente
 * efetuar um saque em um caixa eletr�nico. Os requisitos b�sicos s�o os
 * seguintes:
 *
 * Entregar o menor n�mero de notas;
 *
 * � poss�vel sacar o valor solicitado com as notas dispon�veis;
 * Saldo do cliente infinito;
 * Quantidade de notas infinito (pode-se colocar um valor finito de c�dulas para
 * aumentar a dificuldade do problema);
 *
 * Notas dispon�veis de R$ 100,00; R$ 50,00; R$ 20,00 e R$ 10,00
 * Exemplos:
 *
 * Valor do Saque: R$ 30,00 � Resultado Esperado: Entregar 1 nota de R$20,00 e 1
 * nota de R$ 10,00.
 *
 * Valor do Saque: R$ 80,00 � Resultado Esperado: Entregar 1 nota de R$50,00 1
 * nota de R$ 20,00 e 1 nota de R$ 10,00.
 *
 * @author Filipe Ribeiro
 *
 */
public class CaixaEletronico {

    /**
     * CONSTANT
     * Todas a notas dispon�veis
     */
    private int[] NOTAS = {
            100,
            50,
            20,
            10
    };

    /**
     * Metodo usado para realizar saques
     *
     * @param Valor a ser sacado
     * @return Notas a serem retiradas no ATM
     */
    public Integer[] sacar(final int valor) {
        Integer valorSaque = valor;
        ArrayList<Integer> saque = new ArrayList<Integer>();

        while (valorSaque != 0) {
            for (Integer nota : NOTAS) {
                if (valorSaque >= nota) {
                    saque.add(nota);
                    valorSaque = valorSaque - nota;
                    break;
                }
            }
        }

        Integer[] array = new Integer[saque.size()];
        saque.toArray(array);
        return array;
    }
}
