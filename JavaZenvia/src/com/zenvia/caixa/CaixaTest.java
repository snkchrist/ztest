package com.zenvia.caixa;

import org.junit.Assert;
import org.junit.Test;

public class CaixaTest {

    @Test
    /**
     * Valor do Saque: R$ 30,00 � Resultado Esperado: Entregar 1 nota de R$20,00
     * e 1 nota de R$ 10,00.
     */
    public void testSaque() {
        CaixaEletronico caixa = new CaixaEletronico();
        Integer saque = 30;
        Integer nota20 = 20;
        Integer nota10 = 10;

        Integer[] notas = caixa.sacar(saque);

        Integer somaSaque = 0;

        for (Integer i : notas) {
            somaSaque += i;
        }

        Assert.assertEquals(nota20, notas[0]);
        Assert.assertEquals(nota10, notas[1]);

        // Assert quantidade de notas
        Assert.assertEquals(2, notas.length);

        // Assert valor sacado est� correto
        Assert.assertEquals(saque, somaSaque);
    }

    @Test
    /**
     * Valor do Saque:R$ 80,00� Resultado
     * Esperado:Entregar
     * 1 nota de R$50,00
     * 1 nota de R$ 20,00
     * 1 nota de R$ 10,00.
     */
    public void testSaque2() {
        CaixaEletronico caixa = new CaixaEletronico();
        Integer saque = 80;
        Integer nota50 = 50;
        Integer nota20 = 20;
        Integer nota10 = 10;
        Integer[] notas = caixa.sacar(saque);

        Integer somaSaque = 0;

        for (Integer i : notas) {
            somaSaque += i;
        }

        // Assert valor notas sacadas
        Assert.assertEquals(nota50, notas[0]);
        Assert.assertEquals(nota20, notas[1]);
        Assert.assertEquals(nota10, notas[2]);

        // Assert quantidade de notas
        Assert.assertEquals(3, notas.length);

        // Assert valor sacado est� correto
        Assert.assertEquals(saque, somaSaque);
    }

    @Test
    /**
     * Valor do Saque:R$ 180,00�
     * Resultado
     * Esperado:Entregar
     * 1 nota de R$100,00
     * 1 nota de R$50,00
     * 1 nota de R$ 20,00
     * 1 nota de R$ 10,00.
     */
    public void testSaqueTodasNotas() {
        CaixaEletronico caixa = new CaixaEletronico();
        Integer saque = 180;
        Integer nota100 = 100;
        Integer nota50 = 50;
        Integer nota20 = 20;
        Integer nota10 = 10;
        Integer[] notas = caixa.sacar(saque);

        Integer somaSaque = 0;

        for (Integer i : notas) {
            somaSaque += i;
        }

        // Assert valor notas sacadas
        Assert.assertEquals(nota100, notas[0]);
        Assert.assertEquals(nota50, notas[1]);
        Assert.assertEquals(nota20, notas[2]);
        Assert.assertEquals(nota10, notas[3]);

        // Assert quantidade de notas
        Assert.assertEquals(4, notas.length);

        // Assert valor sacado est� correto
        Assert.assertEquals(saque, somaSaque);
    }

    @Test
    /**
     * Valor do Saque:R$ 250,00�
     * Resultado
     * Esperado:Entregar
     * 2 nota de R$100,00
     * 1 nota de R$50,00
     */
    public void testSaqueUsandoDuasNotas() {
        CaixaEletronico caixa = new CaixaEletronico();
        Integer saque = 250;
        Integer nota100 = 100;
        Integer nota50 = 50;


        Integer[] notas = caixa.sacar(saque);

        Integer somaSaque = 0;

        for (Integer i : notas) {
            somaSaque += i;
        }

        // Assert quantidade de notas
        Assert.assertEquals(3, notas.length);

        // Assert valor notas sacadas
        Assert.assertEquals(nota100, notas[0]);
        Assert.assertEquals(nota100, notas[1]);
        Assert.assertEquals(nota50, notas[2]);

        // Assert valor sacado est� correto
        Assert.assertEquals(saque, somaSaque);
    }

}
